FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# renovate: datasource=github-releases depName=pixelfed/pixelfed versioning=semver extractVersion=^v(?<version>.+)$
ARG PIXELFED_VERSION=0.12.4

RUN curl -L https://github.com/pixelfed/pixelfed/archive/refs/tags/v${PIXELFED_VERSION}.tar.gz | tar -xz --strip-components=1 -C /app/code

ENV ENABLE_CONFIG_CACHE=false
RUN chown -R www-data:www-data /app/code && \
    chmod -R g+w bootstrap/cache && \
    sudo -u www-data composer install --no-ansi --no-interaction --prefer-dist --no-autoloader --ignore-platform-reqs && \
    sudo -u www-data composer dump-autoload --no-scripts --optimize

# Patch directories
RUN ln -fs /app/data/env.production /app/code/.env && \
    mv /app/code/storage /app/code/storage.orig && ln -s /app/data/storage /app/code/storage && \
    rm -rf /app/code/bootstrap/cache && ln -s /run/pixelfed/bootstrap-cache /app/code/bootstrap/cache && \
    ln -s /app/code/storage/app/public /app/code/public/storage

RUN sed -e 's,sleep,//sleep,' -i /app/code/database/migrations/2022_04_20_061915_create_conversations_table.php

# configure nginx
RUN rm /etc/nginx/sites-enabled/* && \
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log && \
    ln -sf /run/php8.3-fpm.log /var/log/php8.3-fpm.log
COPY nginx/pixelfed.conf /etc/nginx/sites-enabled/pixelfed.conf
COPY nginx/readonlyrootfs.conf /etc/nginx/conf.d/readonlyrootfs.conf

RUN crudini --set /etc/php/8.3/fpm/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/8.3/fpm/php.ini PHP post_max_size 64M && \
    crudini --set /etc/php/8.3/fpm/php.ini PHP memory_limit 128M && \
    crudini --set /etc/php/8.3/fpm/php.ini Session session.save_path /run/pixelfed/sessions && \
    crudini --set /etc/php/8.3/fpm/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.3/fpm/php.ini Session session.gc_divisor 100

RUN ln -s /app/data/php.ini /etc/php/8.3/cli/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.3/fpm/conf.d/99-cloudron.ini

# lock www-data but allow su - www-data to work
RUN passwd -l www-data && usermod --shell /bin/bash --home /app/data www-data

# configure supervisor
ADD supervisor/ /etc/supervisor/conf.d/
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf

# Prepare everything to place config under /app/data
COPY env.production start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
