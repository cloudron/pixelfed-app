#!/bin/bash

set -eu

echo "==> Creating directories"
mkdir -p /app/data/public /app/data/storage /run/pixelfed/sessions /run/pixelfed/bootstrap-cache /run/pixelfed/logs /run/pixelfed/framework-cache /run/php

[[ -z "${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-}" ]] && export CLOUDRON_MAIL_FROM_DISPLAY_NAME=Pixelfed

export ENABLE_CONFIG_CACHE=true

# Copy .env to /app/data to make it editable by the user
if [[ ! -f /app/data/env.production ]]; then
    echo "==> Copying env on first run"
    cp /app/pkg/env.production /app/data/env.production

    cp -r /app/code/storage.orig/* /app/data/storage

    echo "==> init database on first run"
    php artisan key:generate # this generates APP_KEY
    php artisan migrate --force

    echo "=> Create the access tokens required for the API"
    php artisan passport:keys
    php artisan passport:client --personal --no-interaction

    echo "==> Importing city data"
    php artisan import:cities

    # Required for the settings page to work (https://github.com/pixelfed/pixelfed/issues/2818)
    echo -e "\nENABLE_CONFIG_CACHE=true" >> /app/data/env.production # should be done after migrate

    echo "==> Creating caches"
    php artisan config:cache
    php artisan route:cache
    php artisan view:cache
else
    echo "==> Creating caches"
    php artisan config:cache
    php artisan route:cache
    php artisan view:cache

    echo "==> Running migrations"
    php artisan migrate --force

    echo "==> Updating pixelfed"
    php artisan update

    php artisan instance:actor
fi

sed -e "s,BROADCAST_DRIVER=.*$,BROADCAST_DRIVER=redis,g" -i /app/data/env.production

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

# sessions, logs and cache
[[ -d /app/data/storage/framework/sessions ]] && rm -rf /app/data/storage/framework/sessions
ln -sf /run/pixelfed/sessions /app/data/storage/framework/sessions
rm -rf /app/data/storage/framework/cache && ln -s /run/pixelfed/framework-cache /app/data/storage/framework/cache
rm -rf /app/data/storage/logs && ln -s /run/pixelfed/logs /app/data/storage/logs

echo "==> Changing ownership"
chown -R www-data:www-data /app/data /run/pixelfed

echo "==> Starting Pixelfed"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i PixelFed
