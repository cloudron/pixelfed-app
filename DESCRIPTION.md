## About

Pixelfed is a free and ethical photo sharing platform, powered by ActivityPub federation.

## Features

* Ad-Free - No ads in timelines, or anywhere
* Chronological - Timelines properly ordered, no algorithms
* Discover - Explore new content and creators
* Filters - Add optional filters to your photos
* Photo Albums - Share your photos, one post at a time
* Privacy Focused - No 3rd party analytics or tracking included

