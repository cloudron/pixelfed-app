#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    let browser, app;
    const username = process.env.USERNAME, password = process.env.PASSWORD + '12345678', email = process.env.EMAIL; // pwd  has to be 12 chars

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function registerUser() {
        await browser.get(`https://${app.fqdn}/register`);
        await waitForElement(By.xpath('//input[@id="name"]'));
        await browser.findElement(By.xpath('//input[@id="name"]')).sendKeys('Covid 19');
        await browser.findElement(By.xpath('//input[@id="username"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@id="email"]')).sendKeys(email);
        await browser.findElement(By.xpath('//input[@id="password"]')).sendKeys(password);
        await browser.findElement(By.xpath('//input[@id="password-confirm"]')).sendKeys(password);
        await browser.findElement(By.xpath('//input[@id="ageCheck"]')).click();
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();
        await waitForElement(By.xpath('//button[contains(text(), "Send Confirmation Email")]'));
        await browser.findElement(By.xpath('//button[contains(text(), "Send Confirmation Email")]')).click();
        await waitForElement(By.xpath('//p[contains(text(), "Verification email sent")]'));
    }

    function verifyUser() {
        execSync(`cloudron exec --app ${app.fqdn} -- bash -c 'mysql --user=\${CLOUDRON_MYSQL_USERNAME} --password=\${CLOUDRON_MYSQL_PASSWORD} --host=\${CLOUDRON_MYSQL_HOST} \${CLOUDRON_MYSQL_DATABASE} -e "UPDATE users SET email_verified_at=NOW();"'`, { encoding: 'utf8', stdio: 'inherit' });
    }

    async function login(email, password) {
        await browser.get(`https://${app.fqdn}/login`);
        await waitForElement(By.xpath('//input[@id="email"]'));
        await browser.findElement(By.xpath('//input[@id="email"]')).sendKeys(email);
        await browser.findElement(By.xpath('//input[@id="password"]')).sendKeys(password);
        await browser.sleep(2000); // it's too fast for me to see otherwise!
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();
        await waitForElement(By.xpath('//p[contains(text(), "This is your home feed")]'));
    }

    async function logout() {
        await browser.get('https://' + app.fqdn);
        await waitForElement(By.xpath('//a[@id="navbarDropdown"]'));
        await browser.findElement(By.xpath('//a[@id="navbarDropdown"]')).click();
        await browser.sleep(5000);
        await browser.findElement(By.xpath('//a[contains(text(), "Logout")]')).click();
        await browser.sleep(4000);
    }

    async function checkSettingsPage() {
        await browser.get('https://' + app.fqdn + '/i/admin/settings');
        await waitForElement(By.id('password'));
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.findElement(By.xpath('//button[contains(text(), "Confirm Password")]')).click();
        await waitForElement(By.xpath('//p[text()="Settings"]'));
    }

    async function makeAdmin() {
        execSync(`echo yes | cloudron exec --app ${app.fqdn} -- sudo -u www-data php artisan user:admin ${username}`, EXEC_ARGS);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can register user', registerUser);
    it('can verify user', verifyUser);
    it('make admin', makeAdmin);
    it('can logout', logout); // logs you in automatically...
    it('can login', login.bind(null, email, password));
    it('check settings page', checkSettingsPage);
    it('can logout', logout);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });
    it('can login', login.bind(null, email, password));
    it('can logout', logout);

    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });
    it('can login', login.bind(null, email, password));
    it('can logout', logout);

    it('can restart app', function () { execSync('cloudron restart --app ' + app.id); });
    it('can login', login.bind(null, email, password));
    it('check settings page', checkSettingsPage);
    it('can logout', logout);

    it('move to different location', function () { execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can login', login.bind(null, email, password));
    it('check settings page', checkSettingsPage);
    it('can logout', logout);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // test update
    it('can install app', function () { execSync('cloudron install --appstore-id org.pixelfed.cloudronapp --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can register user', registerUser);
    it('can verify user', verifyUser);
    it('make admin', makeAdmin);
    it('can logout', logout);

    it('can update', function () { execSync('cloudron update --app ' + LOCATION, EXEC_ARGS); });
    it('can login', login.bind(null, email, password));
    it('check settings page', checkSettingsPage);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });
});
