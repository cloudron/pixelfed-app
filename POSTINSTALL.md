On first visit, register yourself and sign in. You may want to [make yourself an admin](https://docs.cloudron.io/apps/pixelfed/#admin) and disable registration to prevent misuse.

Federation is enabled by default. Check the [federation docs](https://cloudron.io/documentation/apps/pixelfed/#federation), if federated searches are not succeeding.

